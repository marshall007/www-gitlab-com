---
layout: markdown_page
title: "Security Controls RACI Chart"
---

## RACI Overview

RACI charts break down the ownership various elements into the following categories:
* Responsible
  * This is the team that performs the work relating to the related control
* Accountable
  * This is the team required to answer for the final result of the control when audited
* Consulted
  * This is the team that has the right and ability to provide feedback relating to the control
* Informed
  * This is the team that will get an "FYI" relating to the control
    * It is worth noting here that since the majority of information at GitLab is publicly viewable, this refers to explicit notification of control information since a broader definition would have every GitLab team marked as "Informed"

### Security Controls RACI Chart

[This chart](https://docs.google.com/spreadsheets/d/1BTr4scba5KsYOUq3aj3pdgDkazKtu4iSqe93XYjVKJQ/edit#gid=0) shows ownership of the [GitLab security controls](./sec-controls.html) as the compliance team views it today. This information will be updated as we work the various GitLab teams and learn more about how each control operates within the company.

If you notice any errors or have any feedback on this chart, please comment on [this issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/217).

For additional information about the below controls, see the [security controls page](./sec-controls.html).
